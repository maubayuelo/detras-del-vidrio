<?php

$artist_1 = [
  'id' => 1,
  'artist_name' => 'Betto*',
  'artist_email' => 'john@example.com',
  'artist_description' => 'Unlike other programming languages, PHP has no command for declaring a variable. It is created the moment you first assign a value to it. A variable can have a short name (like x and y).',
  'artist_portrait_image' => 'https://via.placeholder.com/100x100?text=Retrato+Artista',
  'work_name' => 'Nombre De La Obra',
  'work_image' => 'https://via.placeholder.com/600x480?text=Obra+Artista',
  'work_description' => 'Lors du confinement, un tsunami de détournements vidéo a déferlé dans les foyers. Mais ces œuvres transformatives, qui remixent des extraits de films, se heurtent encore à la législation sur les droits d’auteur.',
  'socialnetwork_fb' => 'https://www.facebook.com/edgar.plata.1441',
  'socialnetwork_ig' => 'https://www.instagram.com/lottoblanco/',
  'socialnetwork_tw' => 'https://twitter.com/JL_MDesconocido',
  'socialnetwork_site' => 'http://www.edgarplata.com/',
  'socialnetwork_yt' => 'https://www.youtube.com/user/mundodesconocido',
  'socialnetwork_vm' => 'https://vimeo.com/seanpecknold',
  'work_iframe' => '03-content_gallery_artist_02',
];

$artist_2 = [
  'id' => 2,
  'artist_name' => 'Caterina Macchi*',
  'artist_email' => 'john@example.com',
  'artist_description' => 'Nací en Barranquilla. Desde siempre he mostrado interés por la maravillosa perspectiva que ofrece el mundo del arte en general, desde la reproducción fotográfica de imágenes, la música, la antropología y con ella sus diversas culturas antropomorfas, la poesía y las artes plásticas en general, de manera que  esas inclinaciones intelectuales me condujeron, poco después de terminar mis estudios de Diseño Gráfico en la U. Tadeo Lozano de Cartagena,  a viajar a Italia. Un recorrido por algunas de sus ciudades en el norte de la península, con la inquietud  de explorar el conocimiento de los detalles y sus secretos  en la elaboración de los accesorios, mi gusto y la predilección por armar elementos con las manos, me llevaron hasta el estudio Arrais, en Florencia, donde decidí tomar clases  bajo la dirección de la diseñadora Luisa Arrais. Al regresar a mi país puse mi experiencia y conocimientos adquiridos en Macorlyn Joyeros, en la ciudad de Bogotá, conjugándolos con nuevos métodos aplicados al aprendizaje en la metalurgia de elementos preciosos, que me han servido como base estructural para  el trabajo que ahora realizo en el que me valgo, con preferencia, de la plata, y de materiales  con piedras semipreciosas como el jade, la amatista, ágatas, perlas y corales de mar, y cristales de roca y piezas artesanales. Desde entonces he profundizado en mis actividades del diseño creando algunas formas orgánicas, trabajadas con mucha dedicación, consiguiendo nuevos efectos en piezas de joyería, en esa actitud de libertad que quiere proyectar accesorios innovadores.',
  'artist_portrait_image' => 'https://via.placeholder.com/100x100?text=Retrato+Artista',
  'work_name' => 'Nombre De La Obra',
  'work_image' => 'https://via.placeholder.com/600x480?text=Obra+Artista',
  'work_description' => 'La inspiración está basada en las máscaras del Carnaval de Barranquilla y el concepto   es origen.   Tome   básicamente   sus   colores,  texturas,   detalles representativos de ellas y la aplicación de piezas en madera  que encierran formas simples con que son montadas algunas piezas de joyería y accesorios en África, ya que parte del legado de estas marcaras viene de nuestros ancestros afrodescendientes. La forma tradicional de las mascaras del carnaval de Barranquilla son zoomorfas, podemos encontrar representaciones de tigres, jirafas, elefantes, ect. La realización de las máscaras está a cargo de unos pocos artesanos y artistas de Galapa, municipio ubicado en el departamento del Atlántico, producto hecho 100% a mano encargados de esta tradición.',
  'socialnetwork_fb' => 'https://www.facebook.com/edgar.plata.1441',
  'socialnetwork_ig' => 'https://www.instagram.com/lottoblanco/',
  'socialnetwork_tw' => 'https://twitter.com/JL_MDesconocido',
  'socialnetwork_site' => 'http://www.edgarplata.com/',
  'socialnetwork_yt' => 'https://www.youtube.com/user/mundodesconocido',
  'socialnetwork_vm' => 'https://vimeo.com/seanpecknold',
  'work_iframe' => '03-content_gallery_artist_03',
];

$artist_3 = [
  'id' => 3,
  'artist_name' => 'Edgar Plata*',
  'artist_email' => 'john@example.com',
  'artist_description' => 'Unlike other programming languages, PHP has no command for declaring a variable. It is created the moment you first assign a value to it. A variable can have a short name (like x and y).',
  'artist_portrait_image' => 'https://via.placeholder.com/100x100?text=Retrato+Artista',
  'work_name' => 'Nombre De La Obra',
  'work_image' => 'https://via.placeholder.com/600x480?text=Obra+Artista',
  'work_description' => 'Lors du confinement, un tsunami de détournements vidéo a déferlé dans les foyers. Mais ces œuvres transformatives, qui remixent des extraits de films, se heurtent encore à la législation sur les droits d’auteur.',
  'socialnetwork_fb' => 'https://www.facebook.com/edgar.plata.1441',
  'socialnetwork_ig' => 'https://www.instagram.com/lottoblanco/',
  'socialnetwork_tw' => 'https://twitter.com/JL_MDesconocido',
  'socialnetwork_site' => 'http://www.edgarplata.com/',
  'socialnetwork_yt' => 'https://www.youtube.com/user/mundodesconocido',
  'socialnetwork_vm' => 'https://vimeo.com/seanpecknold',
  'work_iframe' => '03-content_gallery_artist_04',
];

$artist_4 = [
  'id' => 4,
  'artist_name' => 'Jose Sierra*',
  'artist_email' => 'john@example.com',
  'artist_description' => 'Unlike other programming languages, PHP has no command for declaring a variable. It is created the moment you first assign a value to it. A variable can have a short name (like x and y).',
  'artist_portrait_image' => 'https://via.placeholder.com/100x100?text=Retrato+Artista',
  'work_name' => 'Nombre De La Obra',
  'work_image' => 'https://via.placeholder.com/600x480?text=Obra+Artista',
  'work_description' => 'En el presente trabajo explora la generación de aquellos estudios en los que se han establecido en que aptitudes son diferentes las personas de uno u otro sexo y cuyos resultados probablemente sabemos parcialmente. Aun cuando en los últimos años se han extendido las investigaciones sobre la construcción social de las diferencias sexuales, considero que continúan las creencias sobre las particularidades de cada sexo, sin base científica alguna y, lo que es más asombroso, continúan influyendo en la conducta manifiesta de hombres y mujeres. Tal parece ser que en la formación de estereotipos y en el mantenimiento de los roles sexuales poco importan los hallazgos científicos. Repetimos, aseveramos y generalizamos frases basándonos en los resultados de ciertos estudios, pero no cuestionamos absolutamente nada acerca de ellos.',
  'socialnetwork_fb' => 'https://www.facebook.com/edgar.plata.1441',
  'socialnetwork_ig' => 'https://www.instagram.com/lottoblanco/',
  'socialnetwork_tw' => 'https://twitter.com/JL_MDesconocido',
  'socialnetwork_site' => 'http://www.edgarplata.com/',
  'socialnetwork_yt' => 'https://www.youtube.com/user/mundodesconocido',
  'socialnetwork_vm' => 'https://vimeo.com/seanpecknold',
  'work_iframe' => '03-content_gallery_artist_05',
];

$artist_5 = [
  'id' => 5,
  'artist_name' => 'Leonor Espinosa',
  'artist_email' => 'john@example.com',
  'artist_description' => 'Unlike other programming languages, PHP has no command for declaring a variable. It is created the moment you first assign a value to it. A variable can have a short name (like x and y).',
  'artist_portrait_image' => 'https://via.placeholder.com/100x100?text=Retrato+Artista',
  'work_name' => 'Nombre De La Obra',
  'work_image' => 'https://via.placeholder.com/600x480?text=Obra+Artista',
  'work_description' => 'Lors du confinement, un tsunami de détournements vidéo a déferlé dans les foyers. Mais ces œuvres transformatives, qui remixent des extraits de films, se heurtent encore à la législation sur les droits d’auteur.',
  'socialnetwork_fb' => 'https://www.facebook.com/edgar.plata.1441',
  'socialnetwork_ig' => 'https://www.instagram.com/lottoblanco/',
  'socialnetwork_tw' => 'https://twitter.com/JL_MDesconocido',
  'socialnetwork_site' => 'http://www.edgarplata.com/',
  'socialnetwork_yt' => 'https://www.youtube.com/user/mundodesconocido',
  'socialnetwork_vm' => 'https://vimeo.com/seanpecknold',
  'work_iframe' => '03-content_gallery_artist_06',
];

$artist_6 = [
  'id' => 6,
  'artist_name' => 'Luis Gonzlez*',
  'artist_email' => 'john@example.com',
  'artist_description' => 'Unlike other programming languages, PHP has no command for declaring a variable. It is created the moment you first assign a value to it. A variable can have a short name (like x and y).',
  'artist_portrait_image' => 'https://via.placeholder.com/100x100?text=Retrato+Artista',
  'work_name' => 'Nombre De La Obra',
  'work_image' => 'https://via.placeholder.com/600x480?text=Obra+Artista',
  'work_description' => 'Lors du confinement, un tsunami de détournements vidéo a déferlé dans les foyers. Mais ces œuvres transformatives, qui remixent des extraits de films, se heurtent encore à la législation sur les droits d’auteur.',
  'socialnetwork_fb' => 'https://www.facebook.com/edgar.plata.1441',
  'socialnetwork_ig' => 'https://www.instagram.com/lottoblanco/',
  'socialnetwork_tw' => 'https://twitter.com/JL_MDesconocido',
  'socialnetwork_site' => 'http://www.edgarplata.com/',
  'socialnetwork_yt' => 'https://www.youtube.com/user/mundodesconocido',
  'socialnetwork_vm' => 'https://vimeo.com/seanpecknold',
  'work_iframe' => '03-content_gallery_artist_07',
];

$artist_7 = [
  'id' => 7,
  'artist_name' => 'Maria Eugenia Trujillo*',
  'artist_email' => 'maritru53@hotmail.com',
  'artist_description' => 'A través de materiales y medios técnicos considerados muchas veces como manualidades: labores de aguja ( el tejido, la costura, el bordado) o la joyería y el modelado, elaboro obras reflexivas y críticas donde se denuncia y transgrede las estructuras patriarcales en forma simbólica; la intención es visualizar los diversos significados de lo qué es ser mujer en la sociedad actual, su capacidad de pensarse a sí misma y mostrarse desde una mirada artística, aunque ello signifique la confrontación y la censura.',
  'artist_portrait_image' => 'https://via.placeholder.com/100x100?text=Retrato+Artista',
  'work_name' => 'Nombre De La Obra',
  'work_image' => 'https://via.placeholder.com/600x480?text=Obra+Artista',
  'work_description' => 'Lors du confinement, un tsunami de détournements vidéo a déferlé dans les foyers. Mais ces œuvres transformatives, qui remixent des extraits de films, se heurtent encore à la législation sur les droits d’auteur.',
  'socialnetwork_fb' => 'https://www.facebook.com/edgar.plata.1441',
  'socialnetwork_ig' => 'https://www.instagram.com/lottoblanco/',
  'socialnetwork_tw' => 'https://twitter.com/JL_MDesconocido',
  'socialnetwork_site' => 'http://www.edgarplata.com/',
  'socialnetwork_yt' => 'https://www.youtube.com/user/mundodesconocido',
  'socialnetwork_vm' => 'https://vimeo.com/seanpecknold',
  'work_iframe' => '03-content_gallery_artist_08',
];

$artist_8 = [
  'id' => 8,
  'artist_name' => 'Patricia Arango*',
  'artist_email' => 'john@example.com',
  'artist_description' => 'Patricia Arango, colombiana, artista, psicóloga y diseñadora, ha residido en Usa, México, Perú y Puerto Rico, nómada por el mundo, profundiza sus intereses por la caligrafía, la simbología de la escritura, la memoria, la percepción y la proyección. Ha expuesto en Puerto Rico, Nueva York, Italia y Colombia. Ha trabajado con diferentes curadores: Tobi Khan crítico de Arte en Nueva York, , Sandra Miranda Pattin de la Bienal de Florencia, Bianca Laura Pettreto del Museo de Arte Contemporáneo de Cerdeña y La Fabbrica Italia, Eduardo Hernández del Museo de Arte Moderno de Cartagena y el maestro Francisco Gil Tovar , reconocido escritor y crítico de arte en Colombia. Es co-fundadora de Maison-Alma, un concepto de Arte Utilitario de Lujo Latinoamericano; que crea piezas de moda de alta costura, utilizando telas de diseño interior francesas y piezas artesanales colombianas de uso en casa.',
  'artist_portrait_image' => 'https://via.placeholder.com/100x100?text=Retrato+Artista',
  'work_name' => 'Nombre De La Obra',
  'work_image' => 'https://via.placeholder.com/600x480?text=Obra+Artista',
  'work_description' => 'Lors du confinement, un tsunami de détournements vidéo a déferlé dans les foyers. Mais ces œuvres transformatives, qui remixent des extraits de films, se heurtent encore à la législation sur les droits d’auteur.',
  'socialnetwork_fb' => 'https://www.facebook.com/edgar.plata.1441',
  'socialnetwork_ig' => 'https://www.instagram.com/lottoblanco/',
  'socialnetwork_tw' => 'https://twitter.com/JL_MDesconocido',
  'socialnetwork_site' => 'http://www.edgarplata.com/',
  'socialnetwork_yt' => 'https://www.youtube.com/user/mundodesconocido',
  'socialnetwork_vm' => 'https://vimeo.com/seanpecknold',
  'work_iframe' => '03-content_gallery_artist_08',
];


global $artist_1;
global $artist_2;
global $artist_3;
global $artist_4;
global $artist_5;
global $artist_6;
global $artist_7;
global $artist_8;

?>
