<nav>
   <div class="menu-icon">
      <div class="ham_icon"></div>
      <div class="ham_icon"></div>
      <div class="ham_icon"></div>
   </div>
   <div class="logo">
      <img src="./dist/images/logo-white.svg"/>
      <img src="./dist/images/logo-black.svg"/>
   </div>
   <div class="menu">
      <ul>
         <li><a class="nav_link" href="#section_hero">Inicio</a></li>
         <li><a class="nav_link" href="#section_about">Exhibición</a></li>
         <li><a class="nav_link" href="#section_gallery">Obras</a></li>
         <li><a class="nav_link" href="#section_comments">Dejar Comentarios</a></li>
         <li><a class="nav_link" href="https://www.facebook.com/Domestikaen/" target="blank"><i class="fa fa-facebook-square"></i></a></li>
         <li><a class="nav_link" href="https://www.facebook.com/Domestikaen/" target="blank"><i class="fa fa-instagram"></i></a></li>
         <li><a class="nav_link" href="https://www.facebook.com/Domestikaen/" target="blank"><i class="fa fa-twitter"></i></a></li>
      </ul>
   </div>
</nav>
