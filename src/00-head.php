<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- <link rel="icon" href="dist/images/favicon.ico"> -->

  <title>Detrás Del Vidrio</title>
  <meta property="og:title" content="The Rock" />
  <meta property="og:url" content="http://www.detras.delvidrio.com" />
  <meta property="og:image" content="https://picsum.photos/id/1019/1280/768" />

  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
  <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <!-- Custom styles for this template -->
  <link href="dist/css/style.css" rel="stylesheet">
  <script src="https://use.fontawesome.com/a1d5edf4b7.js"></script>


</head>
