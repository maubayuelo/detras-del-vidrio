
<!DOCTYPE html>
<html lang="en">
  <?php require '03-content_gallery_artists.php';?>
  <?php include '00-head.php';?>
  <body>
    <?php include '01-header.php';?>
    <?php include '02-hero-banner.php';?>
    <?php include '03-content_about.php';?>
    <?php include '03-content_gallery.php';?>
    <?php include '03-content_gallery-modal.php';?>
    <?php include '03-content_comments.php';?>
    <?php include '04-footer.php';?>
  </body>
</html>
