<div class="anchor_lander" id="section_about"></div>
<section class="section section_about">
  <div class="container">
    <h2 class="text-big text-center text-uppercase m-none">Exhibición</h2>
    <h2 class="text-medium text-center text-uppercase m-none">Sed malesuada ipsum et luctus</h2>
    <div class="row mt-big"> <!-- First level row -->
      <div class="col-md-6 col-sm-12 aligner aligner--centerHoritzontal aligner--centerVertical">
        <img class="img-responsive" src="https://sierra-library.github.io/img/img6.jpg" alt="Image 1">
      </div>
      <div class="col col-md-6 col-sm-12">
        <p class="mt-medium">
          Nunc sed tortor at elit blandit consequat nec a leo. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce quis sapien justo. Nullam dictum porttitor dolor.   Pellentesque a odio tristique, efficitur ex gravida, faucibus est. Fusce eget leo sit amet magna porta tempus.
        </p>
        <p>
          Donec elit arcu, consectetur quis condimentum at, porttitor vitae ex. Nunc a massa sit amet nisi consequat semper non eu dui. Etiam molestie, erat vitae commodo pellentesque, lectus nisi tempus ex, vel hendrerit erat risus id nibh.
        </p>
        <p class="text-uppercase text-italic">
          Eduardo Hernandez - Curador
        </p>
      </div>
    </div>
  </div>
</section >
