


<?php for ($i = 1; $i <= 8; $i++) { ?>
  <?php $artist = ${'artist_'.$i} ?>

  <div id="jsModal_<?php echo $i ?>" class="modal">



    <div class="modal__overlay jsOverlay"></div>


    <div class="modal__container">

      <button class="modal__close jsModalClose">&#10005;</button>

      <div>
        <section class="container-medium">
          <h3 class="text-big text-uppercase"><?php echo $artist['work_name'] ?></h3>
          <p class="text-small m-none">Por</p>
          <h3 class="text-big"><?php echo $artist['artist_name'] ?></h3>
          <p>
            <?php echo $artist['work_description'] ?>
          </p>
          <?php if ($artist['work_iframe']){ ?>
            <?php //echo ('03-content_gallery_artist_'.$i.'.php');?>
            <?php include ('03-content_gallery_artist_'.$i.'.php');?>
          <?php } ?>
        </section>
      </div>

      <div class="artist_section">
        <div class="container-medium">
          <p class="text-small m-none">Acerca de</p>
          <h3 class="text-big m-none"><?php echo $artist['artist_name'] ?></h3>
          <img class="artist_portrait rounded" src="<?php echo $artist['artist_portrait_image'] ?>"/>
          <p>
            <?php echo $artist['artist_description'] ?>
          </p>
          <p class="text-small">Seguir en</p>
          <ul class="social_icons aligner aligner--centerHoritzontal aligner--centerVertical">
            <?php if ($artist['socialnetwork_fb'])  { ?>
              <li><a href="<?php echo $artist['socialnetwork_fb'] ?>" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
            <?php } ?>
            <?php if ($artist['socialnetwork_ig']) { ?>
              <li><a href="<?php echo $artist['socialnetwork_ig'] ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
            <?php } ?>
            <?php if ($artist['socialnetwork_tw'])  { ?>
              <li><a href="<?php echo $artist['socialnetwork_tw'] ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <?php } ?>
            <?php if ($artist['socialnetwork_site'])  { ?>
              <li><a href="<?php echo $artist['socialnetwork_site'] ?>" target="_blank"><i class="fa fa-globe"></i></a></li>
            <?php } ?>
            <?php if ($artist['socialnetwork_yt'])  { ?>
              <li><a href="<?php echo $artist['socialnetwork_yt'] ?>" target="_blank"><i class="fa fa-youtube"></i></a></li>
            <?php } ?>
            <?php if ($artist['socialnetwork_vm'])  { ?>
              <li><a href="<?php echo $artist['socialnetwork_vm'] ?>" target="_blank"><i class="fa fa-vimeo"></i></a></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
<?php } ?>
