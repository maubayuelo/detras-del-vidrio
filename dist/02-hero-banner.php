<div id="section_hero"></div>
<header>
  <h1 class="text-huge text-white text-withSubtitle text-uppercase">Massa porttitor venenatis</h1>
  <h2 class="text-big text-white mb-medium text-uppercase">Class aptent taciti socis</h2>
  <p  class="text-white text-center">
    Sed malesuada ipsum et luctus volutpat. Sed aliquet urna scelerisque massa porttitor venenatis.
  </p>
  <a class="button button--outlined" href="#section_gallery">Conocer Obras <i class="fa fa-chevron-down"></i></a>
</header>
