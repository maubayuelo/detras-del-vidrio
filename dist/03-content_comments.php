<div class="anchor_lander" id="section_comments"></div>
<section class="section section_comments">
  <div class="container-medium">
    <h2 class="text-big text-center text-uppercase m-none">Libro de comentarios</h2>
    <h2 class="text-medium text-center text-uppercase m-none"> Pellentesque a odio tristique, efficitur ex gravida</h2>
    <div class="row mt-big"> <!-- First level row -->
      <div class="col col-sm-12 col-md-6">
        <label class="label text-small text-uppercase mb-small" for="test10">Full Name*</label>
        <div class="input input-fullWidth">
          <input id="test10" placeholder="Input" type="text">
        </div>
        <p class="text-error text-small hidden">Error message</p>
      </div>
      <div class="col col-sm-12 col-md-6">
        <label class="label text-small text-uppercase mb-small" for="test10">E-mail*</label>
        <div class="input input-fullWidth has-error">
          <input id="test10" placeholder="Input" type="text">
        </div>
        <p class="text-error text-small">Error message</p>
      </div>
    </div>
    <div class="row">
      <div class="col-xl">
        <label class="label text-small text-uppercase mb-small" for="message">Your comments</label>
        <div class="textarea  textarea-fullWidth">
          <textarea id="message"></textarea>
        </div>
        <p class="text-error text-small hidden">Error message</p>
      </div>
    </div>
    <div class="row aligner aligner--centerHoritzontal aligner--centerVertical mt-medium">
        <button class="button button--primary text-center m-none">Enviar</button>
    </div>
  </div>
</section >
