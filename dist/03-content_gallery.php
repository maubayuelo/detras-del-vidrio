<div class="anchor_lander" id="section_gallery"></div>
<section class="section section_gallery bg-gray-light">
  <div class="container">
    <h2 class="text-big text-center text-uppercase m-none">Obras</h2>
    <h2 class="text-medium text-center text-uppercase m-none">Sed malesuada ipsum et luctus</h2>
  </div>

  <div class="container text-center  pt-huge pb-big section_carousel hidden-md-up">
    <div class="carousel">
      <?php for ($i = 1; $i <= 8; $i++) { ?>
          <?php $artist = ${'artist_'.$i} ?>
          <div>
            <a class="fancybox jsModalTrigger" href="?id=<?php echo $artist['id'] ?>" rel="group" id="popup">
              <img src="<?php echo $artist['work_image'] ?>">
            </a>
            <h3 class="text-uppercase text-uppercase">
              <a href="?id=<?php echo $artist['id'] ?>" id="popup" class="jsModalTrigger">
                <?php echo $artist['work_name'] ?>
              </a>
            </h3>
            <h4 class="mb-medium text-bold text-uppercase text-small">
              <a href="?id=<?php echo $artist['id'] ?>" id="popup" class="jsModalTrigger">
                <?php echo $artist['artist_name'] ?>
              </a>
            </h4>
          </div>
      <?php } ?>
    </div>
  </div>


  <div class="container text-center hidden-sm-down">
      <div class="row image_grid pt-huge">
        <?php for ($i = 1; $i <= 8; $i++) { ?>
          <?php $artist = ${'artist_'.$i} ?>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
              <a class="fancybox jsModalTrigger" href="#jsModal_<?php echo $artist['id'] ?>" rel="group" id="popup" >
                <img class="img-responsive" src="<?php echo $artist['work_image'] ?>">
              </a>
              <h3 class="text-uppercase text-uppercase">
                <a href="#jsModal_<?php echo $artist['id'] ?>" id="popup" class="jsModalTrigger">
                  <?php echo $artist['work_name'] ?>
                </a>
              </h3>
              <h4 class="mb-medium text-bold text-uppercase text-small">
                <a href="#jsModal_<?php echo $artist['id'] ?>" id="popup" class="jsModalTrigger">
                  <?php echo $artist['artist_name'] ?>
                </a>
              </h4>
          </div>
        <?php } ?>
      </div>
  </div>
</section>




<!-- #section_gallery #jsModal -->
