<section class="section_footer pt-huge pb-huge bg-gray-light">
  <div class="container text-center">
    <ul class="social_icons aligner aligner--centerHoritzontal aligner--centerVertical">
        <li><a href="https://www.facebook.com/Domestikaen/" target="blank"><i class="fa fa-facebook-square"></i></a></li>
        <li><a href="https://www.facebook.com/Domestikaen/" target="blank"><i class="fa fa-instagram"></i></a></li>
        <li><a href="https://www.facebook.com/Domestikaen/" target="blank"><i class="fa fa-twitter"></i></a></li>
    </ul>
    <p class="text-small m-none">
      Colectivo Detrás Del Vidrio, Copyright &copy; <?php echo date("Y"); ?>.<br/>
      Todos los derechos reservados
    </p>
  </div>
</section>
<a href="#" class="cd-top text-replace js-cd-top aligner aligner--centerHoritzontal aligner--centerVertical"><i class="fa fa-arrow-up"></i></a>

<!-- Optional JavaScript -->
<script type="text/javascript" src="dist/js/slick.carousel.min.js"></script>
<script type="text/javascript" src="dist/js/modal.js"></script>
<script type="text/javascript" src="dist/js/script.js"></script>
