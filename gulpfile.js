const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();


// Compile Scss to CSS
function style(){
  // scss location
  return gulp.src('./src/assets/scss/style.scss')
  // compile Scss
  .pipe(sass().on('error', sass.logError))
  // CSS file compiled destination
  .pipe(gulp.dest('./dist/css'))
  // Stream changes to all browsers
  .pipe(browserSync.stream())
}

// compile JS
function js_scripts(){
  // scss location
  return gulp.src('./src/assets/js/*.js')
  // CSS file compiled destination
  .pipe(gulp.dest('./dist/js'))
  // Stream changes to all browsers
  .pipe(browserSync.stream())
}

// compile PHP
function php_files(){
  // scss location
  return gulp.src('./src/**/*.php')
  // CSS file compiled destination
  .pipe(gulp.dest('./dist/'))
  // Stream changes to all browsers
  .pipe(browserSync.stream())
}


function watch(){
  browserSync.init({
      proxy: "http://detrasdelvidrio.local/"
  });
  gulp.watch('./src/assets/scss/**/*.scss', style);
  gulp.watch('./src/assets/js/**/*.js', js_scripts);
  gulp.watch('./src/*.php').on('change', php_files);
}


exports.style = style;
exports.watch = watch;
